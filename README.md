Real Estate Management System
APPTechnology Experts, Inc.

Context:
	Real Estate Management System that is running under SCP
	Adapting the Fiori principles and loosely-coupled system to provide enhanced User Experience
	Out-of-the-box application to cater Real Estate companies in the Philippines

Scope:
	Quotations
	Reservations
	Contracts
	Payments (Cash, Checks, Credit Card)
	Revenue Recognition
	Customer, Unit, Tax, Pricelist, Project Master Data
	Broker and Agent Master Data
	Commissions and Incentives
	Analytics

Tech Stack:
	SAPUI5 (Frontend)
	SAP HANA XSJS (Middleware)
	SAP Business One / ByDesign / HANA (Backend)
	SAP Cloud Platform (PaaS)
	
	